package com.pajakku.eppt.user.repository;

import com.pajakku.eppt.user.domain.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by avew on 7/26/17.
 */
public interface MerchantRepository extends JpaRepository<Merchant, String> {
}
