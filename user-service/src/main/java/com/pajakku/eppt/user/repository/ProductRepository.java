package com.pajakku.eppt.user.repository;

import com.pajakku.eppt.user.domain.Merchant;
import com.pajakku.eppt.user.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by avew on 7/26/17.
 */
public interface ProductRepository extends JpaRepository<Product, String> {
    public Product findBySlug(String slug);

}
