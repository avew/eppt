package com.pajakku.eppt.user.rest;

import com.pajakku.eppt.user.domain.Product;
import com.pajakku.eppt.user.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by avew on 7/26/17.
 */
@RestController
public class ProductResource {
    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/product")
    public Page<Product> semuaProduct(Pageable page) {
        return productRepository.findAll(page);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{slug}")
    public Product productBySlug(@PathVariable String slug) {
        return productRepository.findBySlug(slug);
    }
}
