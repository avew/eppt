package com.pajakku.eppt.user.repository;

import com.pajakku.eppt.user.domain.Product;
import com.pajakku.eppt.user.domain.ProductPhoto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by avew on 7/26/17.
 */
public interface ProductPhotoRepository extends JpaRepository<ProductPhoto, String> {
    public List<ProductPhoto> findByProduct(Product p);

}
