package com.pajakku.eppt.user.rest;

import com.pajakku.eppt.user.domain.Product;
import com.pajakku.eppt.user.domain.ProductPhoto;
import com.pajakku.eppt.user.repository.ProductPhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by avew on 7/26/17.
 */
@RestController
public class ProductPhotoResource {

    @Autowired
    private ProductPhotoRepository productPhotoRepository;

    @RequestMapping(value = "/product/{product}/photos", method = RequestMethod.GET)
    public List<ProductPhoto> semuaProductPhoto(@PathVariable("product") Product p) {
        return productPhotoRepository.findByProduct(p);
    }

}
